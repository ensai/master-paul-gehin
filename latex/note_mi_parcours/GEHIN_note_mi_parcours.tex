\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[left=42.00mm, right=28.00mm, top=35.00mm, bottom=49.00mm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\author{Paul Géhin}
\title{Note de mi-parcours}
\date{25 mai 2021}
\begin{document}
\pagestyle{empty}

\maketitle
\thispagestyle{empty}

\section{La mission}

\subsection{Contexte}



Tous les ans, environ 8 millions de bulletins individuels sont recueillis lors du recensement.
Parmi ceux-ci, 1.7 millions sont sélectionnés pour un traitement statistique poussé.
En particulier, on veut connaître leur lieu de travail et leur profession.

Actuellement, cela se fait en deux parties : batch et reprise manuelle.\\
La partie batch utilise les moteurs MCA (mise en cohérence automatique pour les codes activités) et Sicore-* (système de codification automatique selon un apprentissage des pays, communes, activités et professions).
Elle permet de coder environ 44 \% des Sirets et 88 \% des professions automatiquement.\\
La partie reprise manuelle se fait via une interface web par des agents de l'INSEE.
Ceci occupe 70 ETP pendant 6 mois.\\
Au final, environ 15 \% des Sirets ne sont pas codés chaque année.

Afin d'améliorer ces résultats, diverses expérimentations ont été lancées (hackathon, POC).\\
Dans ce cadre, l’Insee a répondu à l’appel à manifestation d’intérêt dédié à l'Intelligence artificielle (AMI IA) lancé par la DITP et la DINSIC (dans le cadre du fonds \og transition numérique de l'État et modernisation de l'action publique \fg{}) pour améliorer cette codification automatique.
Le sujet étant reconnu d’intérêt général, l’Insee a fait appel à un prestataire, pour développer un algorithme de codification automatique sur le principe d'un moteur de recommandation avec boucle de rétroaction.
L'objectif était de tester les approches de machine learning dans ce problème du codage automatique et de refaire l'application de reprise manuelle actuelle pour intégrer des technologies plus récentes.
Le marché DINSIC a été notifié pour une durée d'un an avec dix mois de travail effectif pour la société.
La première livraison a été effectuée en janvier 2021.

Afin d'en vérifier la fiabilité et les performances, une expérimentation du nouvel outil à grande échelle est en cours.
Cette expérimentation est pilotée par le SSP Lab et réalisée par le prestataire extérieur Starclay, en lien avec le département de la démographie.
Plusieurs établissements régionaux participent à l’opération.
\`{A} cause du coronavirus, l'enquête annuelle de recensement (EAR) a dû être repoussée cette année.
Ainsi, l’appel à candidature pour l'expérimentation a pû être lancé conjointement pour l’ensemble des travaux de substitution faisant suite au report de l’EAR 2021.
Le début de l’évaluation a débuté mercredi dernier (le 19 mai), mobilise 15 ETP et durera environ 2 mois.


\subsection{Résultats attendus}

\begin{itemize}
	\item S'approprier l'algorithme
	\item Suivre l'expérimentation
	\item Étudier la faisabilité d'une mise en production
	\item Proposer des axes d'améliorations
	(algorithme, architecture, etc.)
\end{itemize}

\section{Timing prévisionnel des différentes étapes}

\begin{tabular}{|c|c|}
	\hline
	Jusqu'à mi-mai & Préparation de l'expérimentation \\
	& Prise en main de l'algorithme \\
	\hline
	& Suivi de l'expérimentation \\
	De mi-mai à mi-juillet & Préparer un scénario de mise en production \\
	& Chercher des améliorations à l'algorithme \\
	\hline
	De mi-juillet à mi-août & Bilan de l'expérimentation \\
	& Proposition d'axes d'améliorations \\
	\hline
\end{tabular}

\section{État des travaux}

\subsection{L'algorithme}

L'algorithme proposé est un réseau de neurones se basant sur le \textbf{distance learning}.
Le réseau est dit \og siamois \fg{}, car les deux inputs (bulletin individuel et description de l'entreprise) sont projetés par le même réseau.

Le training se fait par batch de $ N $ paires d'inputs.\\
On a alors une matrice de distance de taille $ N \times N $ composée de 1 à l'exception de la diagonale composée de 0.

Le problème se pose au runtime, où le match entre un bulletin individuel et un des 8 millions de Siret est complexe.
La solution proposée est de s'aider de la projection de la profession, du géocodage de bulletin individuel et des informations renseignées par l'interrogé afin de présélectionner des Siret.
Le modèle peut alors renvoyer des top-k sur cette présélection.

\subsection{L'architecture}

L'application repose sur une approche micro-service conteneurisé.\\
Le build et le déploiement sont automatisés par les runners gitlab.

L'application est hébergée sur des serveurs Marathon de l'innovation.
La fermeture du service innovation est prévue le 1er juin.
Néanmoins, une dérogation nous a été accordée pour mener l'expérimentation à son terme.\\
Il sera donc indispensable de concevoir une solution de déploiement et d'hébergement s'inscrivant dans les standards de l'Insee. 
Une des solutions à étudier est l'utilisation d'un cluster Kubernetes.
Cela permettrait de garder l'architecture conteneurisée.
Cette dernière est actuellement en cours d'expérimentation par l'Insee.

\includegraphics[width=\textwidth]{aiee.png}



\subsection{L'expérimentation}

L'expérimentation utilise les données de l'enquête 2020.\\
Les champs d'intérêts ont été vidés.

$ 75 000 $ bulletins ont été tirés dans les départements 14, 63 et 92.\\
Pour être tirés, les bulletins ont été classés selon leur probabilité de codage automatique du Siret.
Ensuite, dix strates correspondants aux déciles ont été formées.
$ 2500 $ ont été tirés dans le premier et dernier décile, $ 5000 $ dans le deuxième et le neuvième et $ 10000 $ dans les autres.

Un accompagnement de l'évaluation de l'expérimentation a été fourni par la DITP/EY dans le cadre de l'AMI IA.\\
Cela a permis de définir:
\begin{itemize}
	\item Un questionnaire en ligne après une semaine de tests
	\item Des entrevues avec plusieurs gestionnaires et chefs de services après les deux mois de tests
	\item Une mesure du numéro d'ordre sélectionné par le codeur
	\item Une mesure du temps de traitement manuel des bulletins
	\item Une mesure des bulletins correctement repris (par rapport aux résultats de l'EAR 2020)
\end{itemize}



\end{document}