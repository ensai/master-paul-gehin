\chapter{Opération codification employeur}

\section{Introduction}


\subsection{Contexte}

Afin d’évaluer les performances de l’expérimentation, il est nécessaire de faire un test de la nouvelle application et de l’algorithme de codification, via une labellisation d’un échantillon de bulletins individuels du recensement.
Ce projet s’inscrit dans une perspective de gain d’efficience pour le recensement.

Ces travaux ne nécessitent pas de formation particulière s’ils sont effectués par des agents des divisions recensement habitués à travailler sur l’application Recap.
Seulement un guide d’utilisation de la nouvelle application et une présentation des consignes de labellisation ont été suffisants pour lancer l’opération.

{ \bf L’évaluation a durée du 19 mai au 15 juillet. }



\subsection{Objectifs}

\begin{enumerate}
	\item Avoir un retour d’expérience sur l’interface développée
	\item \'{E}tudier la pertinence des échos proposés dans l’application
	\item \'{E}valuer les gains liés à la méthode d’apprentissage statistique sur les futures codifications
\end{enumerate}

\subsubsection*{Objectifs stratégiques}

Cette nouvelle application doit permettre une amélioration de la précision des statistiques domicile-travail.

De plus, il est envisageable que ces nouvelles données du recensement puissent enrichir le répertoire Sirene dans une demarche de ``crowdsourcing''.

\subsubsection*{Objectifs opérationnels}

De nombreuses démonstrations d'améliorations sont attendues de cette opération.
Les principales attendues sont :

\begin{itemize}[label=\faMinus]
	\item l'augmentation de la part des bulletins traités automatiquement.
	\item l'augmentation de la productivité des gestionnaires pour la tâche de reprise de l’établissement employeur
	\item l'augmentation du nombre d’échos pertinents pour les bulletins à reprendre manuellement
	\item l'amélioration de l’exactitude de la reprise par les gestionnaires
\end{itemize}

\subsection{Fonctionnalités}

L'opération a pour objectif de tester en conditions réelles une partie de la solution apportée.

En particulier, on s'intéresse particulièrement à la partie codification du Siret.
Pour se faire, on utilise le nouveau service de prédiction ainsi qu'une nouvelle interface de reprise (modernisation de Recap).

\subsubsection*{Service de prédiction}

\noindent Le service de prédiction calcule trois points :
\begin{enumerate}
	\item le taux de confiance pour la classification en codable / non codable.
	\item le top 10 des Siret
	\item le taux de confiance en la classification automatique du Siret (top 1)
\end{enumerate}

\subsubsection*{Interface de reprise}

L'interface de reprise doit reprendre les fonctionnalités principales de Recap.
Elle sera à compléter par la suite s'il est décidé d'une mise en production.
Elle doit également permettre de nouvelles fonctionnalités :

\begin{itemize}[label=\faMinus]
	\item L'interface permet de mettre en pause la reprise d'un BI.
	\item L'interface inclut un outil de suivi des BI traités.
	\item L'interface doit permettre de fournir des recommandations de Siret en fonction des champs remplis manuellement.
\end{itemize}

\section{Méthode}

\subsection{Données}

Les données sont issues de l'enquête de recensement 2020.

Elles concernent les départements 92, 63, 14 et 57.

Dans un premier temps, la MOA a formulée le souhait d'inclure des données qui avaient de fortes chances d'être non codable dans ce jeu.
Les champs de raison sociale, d'activité et d'adresse notamment ne sont pas remplis.
Cependant, l'algorithme d'attribution des BI à reprendre a fait ressortir ces bulletins.
On a donc eu dans les premières semaines beaucoup de BI jugé non codable.
Par la suite, nous avons exclus ceux-ci.

\subsection{Déroulement}

La recette de l’application et un premier test avec des utilisateurs de la DR Ile-de-France ont été effectués sur un échantillon de 1k de BI.
Un entretien qualitatif avec un gestionnaire et le chef de la section ``Enquêtes de recensement'' de la Direction Régionale d’Ile-de-France a permis de récolter les premières impressions.
De même, un entretien avec la responsable et deux agents de la Division Qualité des Traitements du SeRN\footnote{Service Recensement National de la Population} a permis de cibler les premières améliorations de l'IHM.

L'opération s'est déroulée avec des gestionnaires de Recap dans 8 directions pilotes durant deux mois.
Des données sur les résultats choisis par les gestionnaires ont été récoltées par l'application (lean time, numéro de la proposition choisie).
De plus, un questionnaire en ligne a été transmis aux gestionnaires participant au test en fin d'opération.

\subsection{Mesures / Indicateurs}

\subsubsection*{Ergonomie et facilité de prise en main de l’interface}

\begin{itemize}[label=\faMinus]
	\item Appréciation qualitative sur la facilité de prise en main et l’ergonomie de la nouvelle interface
\end{itemize}

\subsubsection*{Impact de l’utilisation de l’outil sur le processus métier}

\begin{itemize}[label=\faMinus]
	\item Appréciation qualitative sur la pertinence des suggestions fournies par l’outil
	\item Évolution du numéro d’ordre de la proposition sélectionnée au cours de l’expérimentation
	\item Temps de traitement manuel d’un bulletin pendant l’expérimentation
	\item Appréciation qualitative sur l’impact de l’utilisation de l’outil sur la tâche de reprise manuelle des bulletins (facilité, rapidité, etc.)
\end{itemize}

\subsubsection*{Impacts de l’évolution induite sur les parties prenantes}

\begin{itemize}[label=\faMinus]
	\item Taux de bulletins correctement repris par les gestionnaires
	\item Appréciation qualitative sur l’impact de l’utilisation de l’outil sur le travail des gestionnaires
	\item Appréciation qualitative sur l’impact de la mise en place de l’outil sur l’organisation
	du travail dans la direction
	\item Estimation quantitative des gains de productivité permis par l’outil
\end{itemize}

\section{Résultats préliminaires}

\subsection{Résultats techniques}

\subsubsection*{Augmentation du taux de bulletins codés automatiquement}

Avec un seuil de codification automatique à 85\% du Siret et du non codable.
Nous avons une précision similaire à l'algorithme MCA/Sicore mais avec un taux de codification automatique supérieur (de 45\% à 51\%).

\subsubsection*{Recommandations aux gestionnaires afin d’accélérer le codage manuel}

\begin{itemize}[label=\faMinus]
	\item Les gestionnaires ont traités presque 57k BI dans le temps estimé pour en traiter 50k
	\item Choix du gestionnaire parmi les proposition Siret~
	\begin{tabular}{|c|c|c|c|}
		\hline
		Top 1 & Top 2 & Top 5 & Top 10 \\
		\hline
		54\% & 62\% & 69\% & 73\% \\
		\hline
	\end{tabular}
\end{itemize}

\subsection{Appréciation qualitative}

{\bf Ergonomie et prise en main de l’outil}

L'application ressemble très fortement à celle utilisée actuellement, avec une prise en main facile ne nécessitant pas une formation particulière.
Cependant des fonctionnalités non reprises seraient pourtant utiles.

Quelques bugs techniques ont été identifiés.
Certains restent encore à corriger.

{\bf Pertinence des échos}

Les échos manquent encore de pertinence, notamment sur des bulletins facilement codables, avec des propositions parfois incohérentes (sans rapport avec la raison sociale, la commune, ou même l’activité).

{\bf Principales fonctionnalités supplémentaires et améliorations souhaitées}

Les gestionnaires aimeraient pouvoir choisir le mode de recherche à partir de l’adresse déclarée dans le questionnaire (pour le cas où la raison sociale inscrite est différente de la dénomination commerciale couramment utilisée).

La possibilité de pouvoir ajouter une base de formation pour accompagner les nouveaux arrivants à la prise en main de l’outil serait appréciée.

Les agents en charge du pilotage aimeraient pouvoir agir sur les bulletins d’un lot affecté à une Direction Régionale et de réattribuer les droits.

L’onglet de suivi des bulletins doit être amélioré avec plus d’informations et de détails.

\subsection{Risques identifiés}

Une présence de bulletins incodables dans l’échantillon test pourraient avoir un impact négatif sur la perception sur les performances de l’outil par les utilisateurs, ne comprenant pas pourquoi ces bulletins n’ont pas été écartés par l’algorithme.

On note également un risque de sorties incohérentes pour les bulletins codés automatiquement (le niveau de risque dépendra du seuil retenu).

\newpage

\section{Discussion}

\subsection*{Succès}

L'association des utilisateurs de la Direction Régionale Ile-de-France au projet a été appréciée.
Ils ont eu l’occasion de partager leurs difficultés rencontrées au quotidien et besoins d’amélioration sur l’application.

La phase de tests au sein des Directions Régionales a bien été anticipée, avec la possibilité pour les chefs de division d’inscrire les travaux dans les plans de charge des agents.

La sollicitation du SeRN pour présenter la phase de tests aux utilisateurs pilotes a permis une présentation plus proche des réalités et attentes métiers.

\subsection*{Difficultés rencontrées}

Communiquer sur le projet et ses bénéfices potentiels au sein de l’organisation et auprès des utilisateurs s'est révélé compliqué.
Deux raisons principales ont été identifiées.
Premièrement, il est complexe de vulgariser les éléments très techniques du projet sur les aspects d’IA.
Il semble qu'un décalage entre la conception idéalisée des utilisateurs sur l’IA et la réalité des performances du POC\footnote{Proof Of Concept} se soit créé.
Enfin, un manque de sensibilisation et d’expérience des parties prenantes sur les projets d’innovation et le caractère de POC des solutions développées s'est fait ressentir.

Le temps très court dédié aux tests de recette de l’application, en raison de retards pris dans les développements techniques, n'a pas permis de régler l’ensemble des problèmes identifiés en amont de l’expérimentation au sein des Directions Régionales.

La répartition des rôles et tâches des membres de l’équipe projet (MOA et équipe innovation) a manquée de clarté.

\subsection*{Apprentissages}

L’utilisation de l’IA ne peut pas se passer de la définition de règles métiers ``en dur'' dans le cadre des développements.
Dans le cas contraire, un modèle performant est très difficile à trouver.

Il est nécessaire de sensibiliser au sein de l’Insee sur l’innovation et le concept de POC.
Cela permet une meilleure adhésion au projet et une plus grande compréhension des limites actuelles de la solution développée.

On a noté l'importance d’intégrer et de prendre en compte dès les premières phases du projet la dimension et les enjeux métiers, en lien avec la MOA, pour faciliter ensuite l’adhésion à l’outil.

Le nouvel outil impliquera obligatoirement une évolution du métier du gestionnaire.
Une phase d'accompagnement sera donc nécessaire.