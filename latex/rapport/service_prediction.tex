\chapter{Service de prédiction}

\section{Contexte}

Pour rappel, tous les ans, environ 8 millions de bulletins individuels (BI) sont recueillis lors du recensement.
Parmi ceux-ci, 1.7 millions sont sélectionnés pour un traitement statistique poussé.

On s’intéresse ici au traitement consistant à encoder les variables liées à l’emploi du répondant.

On dispose de champs textuels, ouverts (non guidés), courts (nom et adresse de l’employeur, activité principale de l’employeur, profession du répondant, etc.) et sujets à une très forte variabilité et contenant souvent des réponses peu fiables (par exemple le nom de la franchise au lieu du franchisé).

L'objectif est de coder:
\begin{itemize}[label=\faMinus]
	\item Le numéro Siret\footnote{\href{https://www.insee.fr/fr/metadonnees/definition/c1841}{Système d'identification du répertoire des établissements}} de l’établissement employant le répondant
	\item Le code NAF\footnote{\href{https://www.insee.fr/fr/metadonnees/definition/c1078}{nomenclature d'activités française}} de l’établissement (ou APET\footnote{\href{https://www.insee.fr/fr/metadonnees/definition/c1888}{Activité principale exercée} pour l'établissement})
	\item Le code PCS\footnote{\href{https://www.insee.fr/fr/metadonnees/definition/c1493}{Professions et catégories socioprofessionnelles}} du répondant
\end{itemize}

\subsection{Existant}



L'application P7 permet de codifier et redresser les données du recensements (individus-logements-liens, création du ménage et de la famille).
Elle est composé d'une partie batch pour le codage et redressement automatique, d'une IHM Recap\footnote{RECodification de l'Activité et de la Profession} utilisée annuellement d'avril à octobre, ainsi que d'une IHM Recap Qualité utilisée tous les 3-4 ans pour mesurer la qualité des codages automatiques et manuels.

\newpage

\subsubsection*{P7 Batch}

Le module batch est composée de :
\begin{itemize}[label=\faMinus]
	\item de la création de la base MCA\footnote{Mise en Cohérence Automatique pour les codes activités} à partir des fichiers du répertoire Sirus\footnote{\href{https://www.insee.fr/fr/metadonnees/source/serie/s1024}{Système d’Immatriculation au Répertoire des Unités Statistiques}}
	\item du codage
	\item du redressement
	\item d'une partie qualité et arbitrage
\end{itemize}

Le codage automatique de l’activité se base sur la recherche de l’établissement employeur.
En effet, les déclarations d’activité sont peu fiables.
Ainsi, on privilégiera la recherche de l’établissement employeur par le biais d’une mise en concordance automatique (MCA) qui associe aux informations du bulletin individuel (nom de l’établissement, lieu de travail notamment) l’établissement le plus concordant parmi ceux trouvés dans une extraction du répertoire Sirus.
En plus de connaître précisément l’activité professionnelle de la personne, les éléments recueillis concernant l’établissement permettent de coder plus facilement la profession.

En cas de doute ou de codage plus incertain (moins bonne évaluation de la pertinence du codage), on s’appuie alors sur la déclaration de l’activité de la personne.

Ainsi, on distingue deux modes de processus automatique de codage : par la MCA uniquement ou par la MCA, appuyée de la codification automatique de l’activité par Sicore\footnote{Système de codification automatique selon un apprentissage des pays, communes, activités et professions permettant de passer d'un libellé, e.g. pompier, à un code, e.g. 533A dans la PCS2003} (MCA + Sicore activité).

Chaque année environ 44\% des Siret sont codés automatiquement.

Le codage automatique de la profession est réalisé à partir de Sicore.
Le libellé de la profession principale donnée par l’individu est traduit en une modalité de la nomenclature PCS en s’appuyant sur d’autres informations issues du questionnaire ou associées à l’établissement employeur. 

Chaque année environ 88\% des professions sont codés automatiquement.

\subsection*{P7 Recap}

En cas d’échec de codification automatique, en DR, des équipes de codeurs affectés au recensement reprennent le codage à l’aide de l’application web Recap.
Cette activité occupe environ 70 ETP pendant 6 mois.

De la même façon que pour le codage automatique, on cherchera d’abord à retrouver l’établissement employeur.
\`{A} défaut, on essayera de retrouver l’activité, selon une nomenclature (NAF ou APET) la plus détaillée possible de 5 (ou à défaut 2) caractères.
Ainsi, on distingue le codage manuel de l’activité par identification de l’établissement employeur ou le codage direct de l’activité.
Pour le codage de la profession, la codification recherchée est au niveau de la PCS uniquement.

Cette campagne permet de coder manuellement environ 45\% des Siret.


{ \bf \noindent Environ 10\% de Siret ne sont pas codés tous les ans car impossibles à traiter\\
	(champs manquants ou mal renseignés)}

\subsection{Premières tentatives}

Un premier hackaton a mis en avant l’intérêt d’introduire des outils tels qu’ElasticSearch pour rechercher des similarités textuelles entre les bulletins individuels et les entreprises.

Dans la continuité du hackaton, un projet a démontré cet intérêt, ses résultats montrant qu’il était possible de faire remonter le bon résultat dans le top 5 dans environ 60\% des cas (40 à 75\% selon la précision du géocodage).

Ces premières tentatives ont permis de démontrer la faisabilité de l'amélioration de la solution actuelle.
Les principales problématiques ont été d'améliorer le top 1 et d'avoir un algorithme de décision du codage automatique.

\subsection{Enjeux}

\begin{enumerate}
	\item Augmenter le taux de codage automatique
	\item Détecter les BI non codables avant la reprise manuelle par Recap
	\item Améliorer le process de reprise manuelle en améliorant la pertinence des suggestions
\end{enumerate}

L'objectif est donc d'avoir moins de BI à reprendre manuellement et de passer moins de temps à reprendre chaque BI.

\section{Présentation}

\subsection{Modèle de détection des non codables}

L'objectif est de de créer un modèle qui prédise si un bulletin est codable ou non avant de transmettre à l'application Recap.

Malgré que chaque année environ 270k bulletins sont traités pour ``rien'', l'objectif principal est d'obtenir le plus d'information possible de l'enquête du recensement.
On va donc privilégier la précision du modèle à son rappel.

Cette détection est un problème de classification avec deux classes très déséquilibrées.
En effet, seulement un peu moins de 2\% du dataset de test est non codable.

\subsubsection*{Approches testées}

Plusieurs approches de machine learning ont été testées dont certaines relevant du deep learning.

Le modèle mlpTransformer a été retenu en fonction de la précision des prédictions.

%tableau centré à taille variable qui s'ajuste automatiquement suivant la longueur du contenu
\begin{table}[!h]
\begin{center}
\begin{tabular}{|l||*{6}{c|}}\hline
	&\makebox[5em]{Vrai négatif}
	&\makebox[5em]{Faux positif}
	&\makebox[5em]{Faux négatif}
	&\makebox[5em]{Vrai positif}
	&\makebox[3em]{\bf Rappel}
	&\makebox[3em]{\bf Précision}\\\hline\hline
	Régression Logistique&771862&2036&47260&8675&0.155091&0.809915\\\hline
	NaiveBaye&722081&51716&29352&26684&{\bf 0.476194}&0.340357\\\hline
	Xgboost&768584&5314&43286&12649&0.226137&0.704170\\\hline
	mlpTransformer&771704&2194&45617&10318&0.184464&{\bf 0.824648}\\\hline
\end{tabular}
\end{center}
\caption{Tableau récapitulatif des solutions}
\end{table}

\newpage

\subsubsection*{Modèle mlpTransformer - en théorie}

{\bf Réseaux de Neurones}

Les réseaux de neurones font partis du machine learning et sont au coeur du deep learning.
Comme leur nom l'indique, ils sont inspirés du fonctionnement des neurones du cerveau humain.
Ils imitent ainsi la manière dont les neurones propage le signal de proche en proche.

Une réseau de neurones artificiel est ainsi composé de couches de neurones artifiels ou perceptrons.
Il contient ainsi une couche d'entrée, une ou plusieurs couches cachées et une couche de sortie\footnote{illustration en annexe~\ref{rdn}}.
Pour un perceptron donné $ k $ qui reçoit $ n $ signaux de la couche précédentes $ x = (x_{1}, \dots, x_{n}) $, des poids associées $ w_{k} = (w_{k,1}, \dots, w_{k, n}) $ et une fonction $ \varphi $ d'activation.
On a alors : $ y_{k} = \varphi\left( \sum_{i=1}^{n} w_{k,i} x_{i} \right) $\\
Généralement, la fonction $ \varphi $ est une fonction de seuil avec une valeur de seuil $ t_{k} $.
Ce qui donne : \[ y_{k} = \mathds{1}_{\mathbb{R}^{+}}\left(t_{k} - w_{k} \cdot x^{T}\right) \]

Afin d'entrainer le modèle, il faut évaluer sa précision.
Pour se faire, on peut introduire une fonction de coût.
Elle est habituellement référée comme la \textit{mean squarred error} (MSE).

Soit :
\begin{itemize}[label=\faMinus, leftmargin=2cm]
	\item $ \forall i \in \llbracket 1, m \rrbracket, \hat{y_{i}} $ la prédiction du réseau
	\item $ \forall i \in \llbracket 1, m \rrbracket, y_{i} $ la valeur réelle
	\item $ m $ la taille de l'échantillon
\end{itemize}
\[ MSE = \dfrac{1}{2m} \sum_{i=1}^{m} \left( \hat{y_{i}} - y_{i} \right)^{2} \]


L'objectif est donc de minimiser cette fonction de coût en ajustant les poids et seuils.
L'algorithme se renforce par descente de gradient.
La MSE descend donc avec chaque exemple d'entraînement jusqu'à converger vers un minimum local.


{\bf Multilayer Perceptron Neural Network}

Cette catégorie de réseaux se compose de plusieurs couches de perceptrons, généralement inter-connectées selon le principe de la propagation directe (feedforward).
Chaque neurone d'une couche a des connexions dirigées vers les neurones de la couche suivante.

Ces réseaux utilisent une variété de techniques d'apprentissage, la plus populaire étant la rétropropagation du gradient (backpropagation).
Dans ce cas, les valeurs de sortie sont comparées à la réponse correcte pour calculer la valeur d'une fonction d'erreur prédéfinie.
Grâce à diverses techniques, l'erreur est ensuite renvoyée dans le réseau.
En utilisant cette information, l'algorithme ajuste les poids de chaque connexion afin de réduire la valeur de la fonction de coût d'une petite quantité.
Après avoir répété ce processus pendant un nombre suffisamment important de cycles d'apprentissage, le réseau converge généralement vers un minimum local de la MSE.
Dans ce cas, on peut dire que le réseau a appris une certaine fonction cible.
Pour ajuster correctement les poids, on applique une méthode générale d'optimisation non linéaire appelée descente de gradient.
Pour cela, le réseau calcule la dérivée de la fonction d'erreur par rapport aux poids du réseau et modifie les poids de manière à ce que l'erreur diminue (en descendant sur la surface de la fonction de coût).
Pour cette raison, la rétropropagation du gradient ne peut être appliquée que sur des réseaux ayant des fonctions d'activation différentiables.


{\bf Transformer}

Un transformer est un modèle d'apprentissage profond qui adopte le mécanisme de l'attention, en pondérant de manière différentielle l'importance de chaque partie des données d'entrée.
Il est principalement utilisé dans le domaine du NLP\footnote{Natural Language Processing (traitement automatique des langues)} et de la CV\footnote{computer vision}.

Les transformers sont conçus pour traiter des données d'entrée séquentielles pour des tâches telles que la traduction et le résumé de texte.

Cependant, les transformers ne traitent pas nécessairement les données dans l'ordre.
Au contraire, le mécanisme d'attention fournit un contexte pour toute position dans la séquence d'entrée.
Par exemple, si les données d'entrée sont une phrase, le transformer n'a pas besoin de traiter le début de la phrase avant la fin.
Au contraire, il identifie le contexte qui confère un sens à chaque mot de la phrase.
Cette caractéristique permet une plus grande parallélisation que les réseaux de neurones et réduit donc les temps d'apprentissage.

Les transformers sont le modèle de choix pour les problèmes de NLP.
La parallélisation supplémentaire de l'apprentissage permet de s'entraîner sur des ensembles de données plus importants que ce qui était possible auparavant.
Cela a conduit au développement de systèmes pré-entraînés tels que BERT\footnote{Bidirectional Encoder Representations from Transformers} et GPT\footnote{Generative Pre-trained Transformer} qui ont été entraînés avec de grands ensembles de données linguistiques et peuvent être affinés pour des tâches spécifiques.

Le transformer adopte une architecture encodeur-décodeur.
L'encodeur est constitué de couches d'encodage qui traitent l'entrée de manière itérative, une couche après l'autre, tandis que le décodeur est constitué de couches de décodage qui font la même chose à la sortie de l'encodeur.

La fonction de chaque couche d'encodage est de générer des encodages qui contiennent des informations sur les parties des entrées qui sont pertinentes les unes par rapport aux autres.
Elle transmet ses encodages à la couche d'encodage suivante comme entrées.
Chaque couche de décodage fait l'inverse, en prenant tous les codages et en utilisant les informations contextuelles qu'ils contiennent pour générer une séquence de sortie.
Pour ce faire, chaque couche d'encodage et de décodage fait appel à un mécanisme d'attention.

Pour chaque entrée, l'attention pondère la pertinence de toutes les autres entrées et en tire parti pour produire la sortie.
Chaque couche de décodeur possède un mécanisme d'attention supplémentaire qui puise des informations dans les sorties des décodeurs précédents, avant que la couche de décodeur ne puise des informations dans les encodages.

Les couches d'encodage et de décodage disposent toutes deux d'un réseau neuronal feedforward pour le traitement supplémentaire des sorties, et contiennent des connexions résiduelles et des étapes de normalisation des couches.


\subsubsection*{Résultat}

Dans le jeu de test d'environ 270k BI non codables : on écarte environ 60k BI dont 49800 effectivement non codables.

Pour cela,
\begin{itemize}[label=\faMinus]
	\item On utilise une représentation ngrams
	\item L’utilisation d'embeddings fasttext améliore les résultats
	\item Pas d'amélioration avec des techniques de rééquilibrage de classes\footnote{SMOTE (Synthetic Minority Oversampling Technique)}
	\item On utilise un seul bloc d’encodeur Transformer
\end{itemize}

\subsection{Modèle de codage des nomenclatures}

Tous les inputs sont:
\begin{itemize}[label=\faMinus]
	\item Textuels
	\item Extrêmement divers car non guidés
	\item Complémentaires - la profession donnée peut dépendre du contexte, les nomenclatures étant très précises
\end{itemize}

Les nomenclatures sont constituées d’arbres dont les enfants sont des sous éléments de leurs parents.
Pour chaque nœud, on connait sa place dans la hiérarchie de la nomenclature et possède une description textuelle associée.

La nomenclature NAF possède environ 2000 nœuds dont 35 feuilles.

La nomenclature PCS, quant à elle, possède environ 575 nœuds dont 85 feuilles.

L'idée est d'essayer de rapprocher les champs déclaratifs des descriptions des codes nomenclatures.

\subsubsection*{Choix technologique}

On décide d'approcher par le paradigme des problèmes de distance learning simplifiés.
Il n'y aura donc pas nécessairement besoin de filtrer à l'exécution.

Lors du training, on apprend au réseau à projeter une entrée vers un vecteur à $ k $ dimensions.
Pour se faire, à chaque batch de $ N  $ paires d’inputs qui se correspondent, le réseau apprend à ``rapprocher'' $ N $ paires de vecteurs qui se correspondent, et à éloigner $ N(N-1) $ paires de vecteurs qui ne se correspondent pas.
Ainsi, on peut calculer la distance cosine entre deux vecteurs prédits pour chaque élément de la paire à comparer.

Lors du runtime, 
on calcule le vecteur correspondant à l’entrée à coder puis la distance à l'ensemble des vecteurs connus.
On peut alors choisir le code correspondant au vecteur le plus proche.

Une illustration se trouve en annexe~\ref{codage_nomenclatures}

\subsubsection*{Modèle}

Le modèle retenu est un modèle Transformer.

On commence par séparer l’information du BI en diverses couches d'embeddings qui s’additionnent
\begin{itemize}[label=\faMinus]
	\item Embeddings du vocabulaire (par exemple Fasttext)
	\item Embeddings du champ $\rightarrow$ tous les tokens du champ reçoivent le même
	\item Embeddings de la position dans le champs $\rightarrow$ tous les 1\ier{} tokens de chaque champ reçoivent le même, etc
\end{itemize}

Puis on aligne les champs entre les deux types d’input.
En sortie, la projection est normalisée.

Plus l'influence de la représentation choisie est riche (et le vocabulaire large), meilleurs sont les résultats.
Les mots sont donc préférables aux trigrammes.
Eux-même préférables aux bigrammes.
On a également remarqués que l'influence des embeddings pré-entrainés (Fasttext) est non négligeable.
De plus, la taille de ces embeddings doit bien être choisie.
Effectivement, si elle est trop grande, l'information a tendance à trop se disperser.
Et si elle est trop petite, le réseau n'arrive pas à apprendre.

Enfin, la taille de la sortie joue sur les performances.
Si trop petite, le modèle ne trouve pas assez d’expressivité par rapport à la distance.
Et si elle est au contraire trop grande, il y a rapidement plus de progression de performance malgré un temps de calcul et des ressources plus élevés.

Finalement, on se retrouve avec un petit modèle.

Beaucoup d’information se retrouve donc dans les embeddings.
\begin{itemize}[label=\faMinus]
	\item 1 bloc encodeur
	\item 3 têtes
	\item 1 seule couche de sortie
\end{itemize}

\begin{table}[!h]
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\hline
			& MSE (training) & \multicolumn{3}{c|}{Top k pre optim} & \multicolumn{3}{c|}{Top k post optim} \\
			\hline
			& & Top-1 & Top-5 & Top-10 & Top-1 & Top-5 & Top-10 \\
			\hline
			NAF & 0.1186 & 37,4\% & 68,7\% & 76,7\% & 55,2\% & 72,1\% & 78,5\% \\
			\hline
			PCS & 0.1126 & 53,4\% & 77,9\% & 85,2\% & 54,0\% & 78,7\% & 85,6\% \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Taux de prédiction correcte par nomenclature}
\end{table}

Ces résultats montrent que les post process sont pertinents pour la NAF.
Principalement, leur effet est de réorganiser les similarités les plus hautes pour que le bon code remonte en première position.

En revanche, ils ne changent quasiment rien à la PCS.

\subsection{Modèle de codage Siret}

On utilise le même modèle siamois, avec une distance plus simple {\bf 1 si l'entreprise et le BI matchent, 0 sinon}.

Une illustration se situe en annexe~\ref{codage_siret}

\subsubsection*{Filtrage par}

\begin{itemize}[label=\faMinus]
	\item Géocodage des entreprises et des BI via des API Insee (BANO, POI)
	\item Requêtes ElasticSearch basées sur
	\begin{itemize}[label=\faMinus]
		\item Similarité textuelle, proximité géographique pour les entreprises
		\item Similarité sur l’ensemble des champs proximité géographique pour les BI déjà codés.
		On en prend ensuite le Siret associé.
	\end{itemize}
\end{itemize}

Une fois codés (via batch ou reprise manuelle), les BI sont ajoutés dans ElasticSearch et peuvent apparaître dans les requêtes futures.

{\bf Performances : Le siret cherché est présent dans au moins un résultat de requête dans 98,5\% des cas.}

\subsubsection*{Meta-modèle}

Pour améliorer les résultats, on combine plusieurs scores en un score final
\[ score\_final = sim\_siret + \alpha \times sim\_naf + \beta \times score\_elasticsearch \]

\begin{itemize}[label=\faMinus]
	\item $sim\_siret$: similarité entre la projection du candidat Siret et le BI
	\item $ sim\_naf $: score du modèle de similarité NAF si le code APET du candidat est dans le top 10 des prédictions Siret (sinon 0)
	\item $ score\_elasticsearch $: nombre de fois où le candidat apparait dans les résultats du filtrage d'ElasticSearch
	\item $ \alpha $ et $ \beta $ sont des coefficients optimisés par une procédure d’exploration de l’espace des valeurs (optuna\footnote{\href{https://optuna.org/}{optuna} est un logiciel d'optimisation automatique des hyperparamètres, particulièrement conçu pour l'apprentissage supervisé}) sur une partie du dataset de test du training.
\end{itemize}

\begin{table}[!h]
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\hline
			& MSE & \multicolumn{3}{c|}{Top k similarité Siret} & \multicolumn{3}{c|}{Top k meta modèle} \\
			\hline
			& (training) & Top-1 & Top-5 & Top-10 & Top-1 & Top-5 & Top-10 \\
			\hline
			Siret + filtr. orig. & 0.144 & 44\% & 74\% & 85\% & 69\% & 81\% & 85\% \\
			\hline
			Siret + filtr. optim. & 0.144 & 45\% & 75\% & 86\% & 75\% & 84\% & 86\% \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Taux de prédiction correcte par filtrage ElasticSearch}
\end{table}

On voit ici le clair apport des scores complémentaires dans l’amélioration des résultats.

On note aussi que le top 10 ne change pas le méta modèle permet de réordonner les candidats pour optimiser le top 1.

Optimisation du filtrage
\begin{itemize}[label=\faMinus]
	\item Les performances du filtrage ont peu changé (ie. le recall du bon Siret)
	\item En revanche le score associé a beaucoup évolué (basé sur le nombre de fois où le bon Siret est remonté), ce qui a permis d’optimiser sa prise en compte dans le méta-modèle.
\end{itemize}

\subsubsection*{Décision de codage automatique}

Un classificateur est entraîné sur les top k afin de décider du codage automatique.

Pour se faire, on utilise une variable ``écart'' entre les scores des top 1 et top 2.
Puis, on apprend à classer sur les similarités top 1 et top 2 et sur cette variable supplémentaire.
Une fois le modèle entraîné, on choisit un seuil de décision permettant la précision / rappel voulus.
On ne code que les prédictions dont le score est supérieur au seuil.

Le modèle utilisé est XGBoost\footnote{Le eXtreme Gradient Boosting est un algorithme glouton qui peut rapidement s'adapter à un ensemble de données d'apprentissage. Des méthodes de régularisation sont donc utilisées pour améliorer les performances de l'algorithme en réduisant le surapprentissage.} (interprétable et de performance élevée).

\begin{table}[!h]
	\begin{center}
		\begin{tabular}{|c|c|c|c|}
			\hline
			& \multicolumn{3}{c|}{Codage automatique} \\
			\hline
			Précision & Total codé & Taux non codé par erreur & Taux codé par erreur \\
			\hline
			0.8 & 94\% & 0.6\% & 18.5\% \\
			\hline
			0.9 & 77\% & 6.9\% & 7.9\% \\
			\hline
			0.95 & 64.5\% & 15\% & 3.5\% \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Taux de codage automatique par précision}
\end{table}

%\section{Mise en production}
%
%Annexe~\ref{archi}
%
%\subsection*{Technologies utilisées}
%
%\begin{itemize}[label=\faMinus]
%	\item Python \faPython
%	\item NodeJS \faNodeJs / React \faReact
%	\item Docker \faDocker / Kubernetes \faDharmachakra
%	\item Gitlab \faGitlab
%\end{itemize}
%
%
%
%\subsubsection*{Service de prédiction \faPython}
%
%\subsubsection*{Backend Recap \faNodeJs/\faReact}
%
%\subsubsection*{Frontend Recap \faReact}
%
%\subsubsection*{Kubernetes \faDharmachakra}
%
%\subsubsection*{Gitlab CI/CD \faGitlab}
%
%\subsection*{Impacts sur la chaîne actuelle}
%
%\subsubsection*{P7 Batch}
%
%\subsubsection*{P7 Recap}